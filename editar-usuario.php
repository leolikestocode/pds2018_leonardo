<?php //include("conexao.php"); ?>
<html lang="en">

<!-- PARA LOGIN -->
<?php
if(isset($_GET["success"])) {
    header("Location: index.php");
}
session_start();
include_once('link.php');
if (isset($_SESSION['email'])){
    $email = $_SESSION['email'];
    $fname = $_SESSION['firstname'];
    $lname = $_SESSION['lastname'];
    $id = $_SESSION['id'];
}

?>

<head>
    <meta http-equiv="content-type" content="text/html;charset=utf-8" />
    <meta name="description" content="">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- The above 4 meta tags *must* come first in the head; any other head content must come *after* these tags -->

    <!-- Title  -->
    <title>Editar Usuário</title>

    <!-- Favicon  -->
    <link rel="icon" href="img/core-img/carona-logo.png">

    <!-- Core Style CSS -->
    <link rel="stylesheet" href="css/core-style.css">
    <link rel="stylesheet" href="style.css">

</head>

<body>

   <?php include_once('header.html'); ?>

    <!-- ##### Breadcumb Area Start ##### -->
    <div class="breadcumb_area bg-img" style="background-image: url(img/bg-img/breadcumb.jpg);">
        <div class="container h-100">
            <div class="row h-100 align-items-center">
                <div class="col-12">
                    <div class="page-title text-center">
                        <h2>editar usuário</h2>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- ##### Breadcumb Area End ##### -->

    <!-- ##### Checkout Area Start ##### -->
    <div class="checkout_area section-padding-80">
        <div class="container">
            <div class="row">

                <div class="col-12 col-md-6">
                    <div class="checkout_details_area clearfix order-details-confirmation">

                        <div class="cart-page-heading mb-30">
                            <h5>Editar Usuário - <?php echo $email; echo " - ". $id;?></h5>
                        </div>

                        <form method="post" action="editar-usuario.php?success=true">
                            <div class="row">
                                <div class="col-md-6 mb-3">
                                    <label for="first_name">Nome <span>*</span></label>
                                    <input type="text" class="form-control" id="NOME" name="NOME" value="<?php echo $fname?>" required>
                                </div> 
                                <div class="col-md-6 mb-3">
                                    <label for="last_name">Sobrenome <span>*</span></label>
                                    <input type="text" class="form-control" id="last_name" name="SOBRENOME" value="<?php echo $lname?>" required>
                                </div>
                                <div class="col-12 mb-4">
                                    <label for="email_address">E-mail <span>*</span></label>
                                    <input type="email" class="form-control" id="email_address" name="EMAIL" value="<?php echo $email?>" required>
                                </div>
                                <div class="col-12 mb-4">
                                    <label for="password">Senha <span>*</span></label>
                                    <input type="password" class="form-control" id="password" name="SENHA" placeholder="digite a senha" required>
                                </div>
                                <div class="col-12 mb-4">
                                    <label for="conf_password">Confirme a Senha <span>*</span></label>
                                    <input type="password" class="form-control" id="conf_password" required>
                                </div>
                                <div class="col-12">
                                    <input type="submit" value="editar" name="edit" class="btn essence-btn cadastro-btn submit"/>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- ##### Checkout Area End ##### -->

    <?php include_once('footer.html'); ?>

    <!-- INCLUDES PHP -->
    <!-- INSERIR DADOS -->
    <?php
        if (isset($_POST['NOME'])){
            $nome = $_POST['NOME'];
            $sobrenome = $_POST['SOBRENOME'];
            $email = $_POST['EMAIL'];
            $senha = $_POST['SENHA'];

            $conn = mysqli_connect("localhost", "root", "","registrationdb");

            $sql = "UPDATE tbluser SET NOME = '$nome', SOBRENOME = '$sobrenome', EMAIL = '$email', SENHA = '$senha' WHERE ID = $id";

            $_SESSION['email'] = $email;
            $_SESSION['firstname'] = $nome;
            $_SESSION['lastname'] = $sobrenome;
            $_SESSION['password'] = $senha;

            if ($conn->query($sql) === TRUE) {
                echo "Editado com sucesso";

            } else {
                echo "Error: " . $sql . "<br>" . $conn->error;
            }            
        }
    ?>

</body>

</html>