<?php
session_start();
if (!isset($_SESSION['email'])){
    header("Location: login-cadastro.php");
}
include_once('link.php');

$idUsuario = $_SESSION['id'];
?>

<!DOCTYPE html>
<html lang="en">


<head>
    <meta charset="UTF-8">
    <meta name="description" content="">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- The above 4 meta tags *must* come first in the head; any other head content must come *after* these tags -->

    <!-- Title  -->
    <title>Solicitar Carona</title>

    <!-- Favicon  -->
    <link rel="icon" href="img/core-img/favicon.ico">

    <!-- Core Style CSS -->
    <link rel="stylesheet" href="css/core-style.css">
    <link rel="stylesheet" href="style.css">

    <link rel="stylesheet" href="//cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">

</head>

<body class="cadastrar-carona-page">
    
   <?php include_once('header.html'); ?>

    <!-- ##### Breadcumb Area Start ##### -->
    <div class="breadcumb_area bg-img" style="background-image: url(img/bg-img/breadcumb.jpg);">
        <div class="container h-100">
            <div class="row h-100 align-items-center">
                <div class="col-12">
                    <div class="page-title text-center">
                        <h2>Solicitar Caronas</h2>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- ##### Breadcumb Area End ##### -->

    <div style="display:block;padding: 30px; margin-left: 50px;" class="table-users">

        <!-- VISUALIZAR DADOS -->
        <?php
            $conn = mysqli_connect("localhost", "root", "","registrationdb");   
            $query = mysqli_query($conn,"SELECT *,(SELECT COUNT(*) FROM tblcaronacadastro_usuario WHERE ID_CARONA = tc.ID and ID_USUARIO = $idUsuario) as INSCRITO FROM `tblcaronacadastro` tc");
        ?>

        <table cellspacing="0" cellpadding="0" width="1040" style="display: block; padding-bottom: 15px;" id="myTable">
            <thead>
            <tr>
                <td width="200" style="font-weight: 600;">
                    NOME
                </td>
                <td width="150" style="font-weight: 600;">
                    PARTIDA
                </td>
                <td width="150" style="font-weight: 600;">
                    CHEGADA
                </td>
                <td width="200" style="font-weight: 600;">
                    DATA
                </td>
                <td width="80" style="font-weight: 600;">
                    QTD
                </td>
                <td width="120" style="font-weight: 600;">
                    AÇÕES
                </td>
                <td width="140" style="font-weight: 600;">
                    AÇÕES 2
                </td>
            </tr>
            </thead>
            
            <tbody>
            <?php
            while($row = mysqli_fetch_assoc($query)) {
                
                ?> <tr><td width="200">  <?php echo $row['NOME'];
                ?> </td><td width="150"> <?php echo $row['PONTOPARTIDA'];
                ?> </td><td width="150"> <?php echo $row['PONTOCHEGADA'];
                ?> </td><td width="200"> <?php echo $row['DATAPARTIDA'];
                ?> </td><td width="80">  <?php echo $row['QTDPASSAGEIROS'];
                ?> </td><td width="120">
                <?php if ($_SESSION['firstname'] != $row['NOME'] && !$row['INSCRITO']){ ?>
                    <a href="solicitar-carona-check.php?id=<?php echo $row['ID'];?>&qtd=<?php echo $row['QTDPASSAGEIROS'];?>">Solicitar</a>
                <?php } ?>
                </td><td width="140">
                <?php if ($row['INSCRITO']){ ?>
                    <a href="diminuir-usuario-carona-check.php?id=<?php echo $row['ID'];?>">Excluir Vaga</a>
                <?php } ?>
                <?php if ($_SESSION['firstname'] == $row['NOME']){ ?>
                    <a href="deletar-solicitar-carona-check.php?id=<?php echo $row['ID'];?>">Excluir Carona</a>
                <?php } ?>

                   </td></tr><?php 
            }

            mysqli_close($conn);
            ?></tbody>
        </table>
    </div>

    <a href="cadastrar-carona.php" class="btn essence-btn" style="margin-left: 50px; margin-bottom: 30px;">Cadastrar Caronas</a>

    <?php include_once('footer.html'); ?>

    <script type="text/javascript" src="//cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>

    <script type="text/javascript">
        $(document).ready(function(){
            $('#myTable').DataTable();
        });
        $(document).ready(function(){
            if (window.location.href.indexOf('success=true') != -1) {
                swal("Solicitação realizada com sucesso");
            }
            if (window.location.href.indexOf('fail=true') != -1) {
                swal("Número máximo de passageiros: 4");
            }
            if ($('td.dataTables_empty').length){
                swal("Nenhuma carona cadastrada");
                setTimeout(function(){ window.location.href = "cadastrar-carona.php"; }, 3000);
            }
        });
    </script>

</body>

</html>