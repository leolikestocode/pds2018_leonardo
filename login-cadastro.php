
<html lang="en">

<head>
    <meta http-equiv="content-type" content="text/html;charset=utf-8" />
    <meta name="description" content="">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- The above 4 meta tags *must* come first in the head; any other head content must come *after* these tags -->

    <!-- Title  -->
    <title>Cadastro - Login</title>

    <!-- Favicon  -->
    <link rel="icon" href="img/core-img/carona-logo.png">

    <!-- Core Style CSS -->
    <link rel="stylesheet" href="css/core-style.css">
    <link rel="stylesheet" href="style.css">

</head>

<body class="login-page">

   <?php include_once('header.html'); ?>

    <!-- ##### Breadcumb Area Start ##### -->
    <div class="breadcumb_area bg-img" style="background-image: url(img/bg-img/breadcumb.jpg);">
        <div class="container h-100">
            <div class="row h-100 align-items-center">
                <div class="col-12">
                    <div class="page-title text-center">
                        <h2>cadastro/login</h2>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- ##### Breadcumb Area End ##### -->

    <!-- ##### Checkout Area Start ##### -->
    <div class="checkout_area section-padding-80">
        <div class="container">
            <div class="row">

                <div class="col-12 col-md-6">
                    <div class="checkout_details_area clearfix order-details-confirmation">

                        <div class="cart-page-heading mb-30">
                            <h5>Cadastre-se</h5>
                        </div>

                        <form action="login-cadastro.php" method="post">
                            <div class="row">
                                <div class="col-md-6 mb-3">
                                    <label for="first_name">Nome <span>*</span></label>
                                    <input type="text" class="form-control" id="first_name" name="NOME" required>
                                </div>
                                <div class="col-md-6 mb-3">
                                    <label for="last_name">Sobrenome <span>*</span></label>
                                    <input type="text" class="form-control" id="last_name" name="SOBRENOME" required>
                                </div>
                                <div class="col-12 mb-4">
                                    <label for="email_address">E-mail <span>*</span></label>
                                    <input type="email" class="form-control" id="email_address" name="EMAIL" required>
                                </div>
                                <div class="col-12 mb-4">
                                    <label for="password">Senha <span>*</span></label>
                                    <input type="password" class="form-control" id="password" name="SENHA" required>
                                </div>
                                <div class="col-12 mb-4">
                                    <label for="conf_password">Confirme a Senha <span>*</span></label>
                                    <input type="password" class="form-control" id="conf_password" required>
                                </div>


                                <div class="col-12">
                                    <div class="custom-control custom-checkbox d-block mb-2">
                                        <!--<input type="checkbox" class="custom-control-input" id="customCheck1">
                                        <label class="custom-control-label" for="customCheck1">Aceitar Termos e Condições</label>-->
                                    </div>
                                </div>
                                <div class="col-12">
                                    <input type="submit" class="btn essence-btn cadastro-btn submit"/>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>

                <div class="col-12 col-md-6 col-lg-5 ml-lg-auto">
                    <div class="order-details-confirmation">

                        <div class="cart-page-heading mb-30">
                            <h5>Login</h5>
                        </div>
                        <form class="form-horizontal" method="POST" action="login_code.php">
                           <div class="mb-4">
                               <label for="email">E-mail</label>
                               <input type="email" class="form-control" name="email" id="email">
                           </div>
                           <div class="mb-4">
                               <label for="pwd">Senha</label>
                               <input type="password" class="form-control" name="password" id="pwd">
                           </div>
                           <input type="submit" name="login" class="btn essence-btn login-btn"/>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- ##### Checkout Area End ##### -->

    <a href="#" class="btn essence-btn see-users" style="margin-left: 50px; margin-bottom: 30px;">Visualizar Usuários</a>

    <div style="display:none;padding: 0 30px 30px; margin-left: 50px;" class="table-users">

        <!-- VISUALIZAR DADOS -->
        <?php
            $conn = mysqli_connect("localhost", "root", "","registrationdb");   
            $query = mysqli_query($conn,"SELECT * FROM tbluser");
        
            ?>
            <table cellspacing="0" cellpadding="0" width="900" style="display: block; padding-bottom: 15px;">
                <tr>
                    <td width="200" style="font-weight: 600;">
                        NOME
                    </td>
                    <td width="200" style="font-weight: 600;">
                        SOBRENOME
                    </td>
                    <td width="300" style="font-weight: 600;">
                        EMAIL
                    </td>
                    <td width="200" style="font-weight: 600;">
                        SENHA
                    </td>
                </tr>

                    <?php
                    while($row = mysqli_fetch_assoc($query)) {
                        
                        ?> <tr><td width="200"> <?php echo $row['NOME'];
                        ?> </td><td width="200"> <?php echo $row['SOBRENOME'];
                        ?> </td><td width="300"> <?php echo $row['EMAIL'];
                        ?> </td><td width="200"> <?php echo $row['SENHA'];
                        ?> </td></tr><?php 
                    }

                    mysqli_close($conn);
                ?></table>
        <a href="#" class="btn essence-btn hide-users" style="margin: 30px 0 0 -30px">Esconder Usuários</a>
    </div>

    <?php include_once('footer.html'); ?>

    <!-- INCLUDES PHP -->
    <!-- INSERIR DADOS -->
    <?php
        if (isset($_POST['NOME'])){
            $nome = $_POST['NOME'];
            $sobrenome = $_POST['SOBRENOME'];
            $email = $_POST['EMAIL'];
            $senha = $_POST['SENHA'];


            $conn = mysqli_connect("localhost", "root", "","registrationdb");
            $sql = "INSERT INTO tbluser (NOME, SOBRENOME, EMAIL, SENHA)
            VALUES ('$nome', '$sobrenome','$email','$senha')";

            echo $sql;

            if ($conn->query($sql) === TRUE) {
                echo "New record created successfully";
            } else {
                echo "Error: " . $sql . "<br>" . $conn->error;
            }

            $conn->close();
        }
    ?>

</body>

</html>