<?php
session_start();
if (!isset($_SESSION['email'])){
    header("Location: login-cadastro.php?login=true");
}
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="description" content="">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- The above 4 meta tags *must* come first in the head; any other head content must come *after* these tags -->

    <!-- Title  -->
    <title>Sobre Nós</title>

    <!-- Favicon  -->
    <link rel="icon" href="img/core-img/carona-logo.png">

    <!-- Core Style CSS -->
    <link rel="stylesheet" href="css/core-style.css">
    <link rel="stylesheet" href="style.css">

</head>

<body>
    <?php include_once('header.html'); ?>

    <!-- ##### Blog Wrapper Area Start ##### -->
    <div class="single-blog-wrapper">

        <!-- Single Blog Post Thumb -->
        <div class="single-blog-post-thumb">
            <img src="img/bg-img/bg-sobre-nos.jpg" alt="">
        </div>

        <div class="container">
            <div class="row justify-content-center">
                <div class="col-12 col-md-8">
                    <div class="regular-page-content-wrapper section-padding-80">
                        <div class="regular-page-text">
                            <h2>COMO TUDO COMEÇOU</h2>
                            <p style="text-align: justify;">
                            Eis que o professor de desenvolvimento de Software nos desafiou a montar um software sozinho, sim eu disse sozinho mesmo. Esse foi um desafio muito bom, pois trabalho em tempo integral como Programador WEB na ACúpula Desenvolvimentos Ltda, sendo apenas de diferente conhecimento pesquisar sobre a linguagem PHP e sua integração com meu software
                            </p>
                            <br/>
                            <h2>Mas foi difícil?</h2>
                            <p>
                            Tive bastante dificuldade no começo do software pois o começo sempre é o mais complicado, mesmo assim pude vencer as barreiras e fazer um trabalho supimpa.
                            </p>
                            <br/>
                            <h2>O que você leva de bom nesse trabalho?</h2>
                            <p>
                            Todo conhecimento a mais é uma arma poderosa no mercado de trabalho, e na vida de um programador quanto mais linguagens conhecidas e entendidas mais chance ele tem de se empregar e ser bem sucedido. Espero que goste de meu trabalho, pois dediquei bastante tempo e esforço nele. Assim que o Carona 10 começou e vai se proliferar no futuro.
                            </p>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- ##### Blog Wrapper Area End ##### -->

    <?php include_once('footer.html'); ?>

</body>

</html>