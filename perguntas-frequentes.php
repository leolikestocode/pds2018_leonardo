<?php
session_start();
if (!isset($_SESSION['email'])){
    header("Location: login-cadastro.php?login=true");
}
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="description" content="">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- The above 4 meta tags *must* come first in the head; any other head content must come *after* these tags -->

    <!-- Title  -->
    <title>FAQ - Perguntas Frequentes</title>

    <!-- Favicon  -->
    <link rel="icon" href="img/core-img/carona-logo.png">

    <!-- Core Style CSS -->
    <link rel="stylesheet" href="css/core-style.css">
    <link rel="stylesheet" href="style.css">

</head>

<body>

    <?php include_once('header.html'); ?>

    <!-- ##### Blog Wrapper Area Start ##### -->
    <div class="single-blog-wrapper">

        <!-- Single Blog Post Thumb -->
        <div class="single-blog-post-thumb">
            <img src="img/bg-img/bg-faq.jpg" alt="">
        </div>

        <div class="container">
            <div class="row justify-content-center">
                <div class="col-12 col-md-8">
                    <div class="regular-page-content-wrapper section-padding-80">
                        <div class="regular-page-text">
                            <h2>1 - Como faço para procurar uma carona?</h2>
                            <p>
                            - Basta procurar no menu o link para solicitar uma carona.<br/> 
                            - Escolha a carona mais conveniente. Como aquela que sai da sua rua e que vai passar exatamente onde você quer chegar.<br/>
                            - Reserve a carona que melhor lhe atende!
                            </p>
                            <br/>
                            <h2>2 - Sou responsável pela bagagem do meu passageiro?</h2>
                            <p>
                            A lei declara que você é responsável, como condutor, por toda a bagagem que se encontra em seu veículo, seja ela de sua propriedade ou de um de seus passageiros.
                            <br/>
                            Isso significa que você pode pedir para abrir e investigar o conteúdo da bagagem de seu passageiro. Nós sabemos que esse não é um pedido prático ou fácil de se fazer, mas é a lei. Não há nenhuma jurisprudência em particular sobre caronas, mas é recomendado tomar as precauções necessárias ao viajar e, trajetos arriscados ou através de fronteiras.
                            </p>
                            <br/>
                            <h2>3 - Preciso conversar durante a viagem?</h2>
                            <p>
                            Somente se desejar! Ao criar seu perfil na BlaBlaCar, é só indicar se você é “Bla”, “BlaBla” ou “BlaBlaBla”, de acordo com o quanto você gosta de conversar no carro. É legal fazer um pequeno esforço para quebrar o gelo no começo da viagem, para que todos se sintam à vontade, mas também não tem problema estar cansado, ou somente quieto, não sendo necessário conversar, se preferir.
                            </p>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- ##### Blog Wrapper Area End ##### -->

    <?php include_once('footer.html'); ?>

</body>

</html>