<?php
session_start();
if (!isset($_SESSION['email'])){
    header("Location: login-cadastro.php?login=true");
}
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="description" content="">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- The above 4 meta tags *must* come first in the head; any other head content must come *after* these tags -->

    <!-- Title  -->
    <title>Conselho de uma jovem mochileira: viaje sozinha!</title>

    <!-- Favicon  -->
    <link rel="icon" href="img/core-img/carona-logo.png">

    <!-- Core Style CSS -->
    <link rel="stylesheet" href="css/core-style.css">
    <link rel="stylesheet" href="style.css">

</head>

<body>

    <?php include_once('header.html'); ?>

    <!-- ##### Blog Wrapper Area Start ##### -->
    <div class="single-blog-wrapper">

        <!-- Single Blog Post Thumb -->
        <div class="single-blog-post-thumb">
            <img src="img/bg-img/blbk1.png" alt="">
        </div>

        <!-- Single Blog Content Wrap -->
        <div class="single-blog-content-wrapper d-flex">

            <!-- Blog Content -->
            <div class="single-blog--text">
                <h2>Conselho de uma jovem mochileira: viaje sozinha!</h2>
                <p>Quando eu era criança tinha medo de ir no quintal de casa sozinha. Pois é. Pouca gente sabe disso, mas meus familiares não me deixariam mentir.</p>
                <p>Meu avô tinha muitas plantas em casa numa espécie de jardim e, na minha imaginação que sempre foi fértil, jurava que estava no meio da floresta da bruxa má! Meu primo mais velho me pegava nos braços e me largava lá perto das plantas só para ter o prazer de me ver voltando aos prantos para o colo da minha avó por estar “sozinha na floresta”.</p>
                <p>Também tinha vergonha de pedir as coisas nos restaurantes, minha mãe sempre me fazia ir até lá e perguntar quanto custava o refrigerante, eu só queria sumir. Ah, também tinha um medo gigantesco de subir no trepa-trepa do parquinho, porque eu parecia muito pequena e ele grande demais. Porém, sobre tudo e mais forte que tudo, tinha medo de ficar sozinha.</p>
                <p>Também tinha vergonha de pedir as coisas nos restaurantes, minha mãe sempre me fazia ir até lá e perguntar quanto custava o refrigerante, eu só queria sumir. Ah, também tinha um medo gigantesco de subir no trepa-trepa do parquinho, porque eu parecia muito pequena e ele grande demais. Porém, sobre tudo e mais forte que tudo, tinha medo de ficar sozinha.</p>

            </div>

            <!-- Related Blog Post -->
            <div class="related-blog-post">
                <!-- Single Related Blog Post -->
                <div class="single-related-blog-post">
                    <img src="img/bg-img/bl1.jpg" alt="">
                    <a href="blog1.php">
                        <h5>Conselho de uma jovem mochileira: viaje sozinha!</h5>
                    </a>
                </div>
                <!-- Single Related Blog Post -->
                <div class="single-related-blog-post">
                    <img src="img/bg-img/bl2.jpg" alt="">
                    <a href="blog2.php">
                        <h5>Livros de montanha e exploração</h5>
                    </a>
                </div>
                <!-- Single Related Blog Post -->
                <div class="single-related-blog-post">
                    <img src="img/bg-img/bl3.jpg" alt="">
                    <a href="blog3.php">
                        <h5>A importância da viagem outdoor</h5>
                    </a>
                </div>
                <!-- Single Related Blog Post -->
                <div class="single-related-blog-post">
                    <img src="img/bg-img/bl4.jpg" alt="">
                    <a href="blog4.php">
                        <h5>Filmes para inspirar sua viagem de mochilão</h5>
                    </a>
                </div>
            </div>

        </div>
    </div>
    <!-- ##### Blog Wrapper Area End ##### -->

    <?php include_once('footer.html'); ?>

</body>

</html>