(function ($) {
    'use strict';

    var $window = $(window);

    // :: Nav Active Code
    if ($.fn.classyNav) {
        $('#essenceNav').classyNav();
    }

    // :: Sliders Active Code
    if ($.fn.owlCarousel) {
        $('.popular-products-slides').owlCarousel({
            items: 4,
            margin: 30,
            loop: true,
            nav: false,
            dots: false,
            autoplay: true,
            autoplayTimeout: 5000,
            smartSpeed: 1000,
            responsive: {
                0: {
                    items: 1
                },
                576: {
                    items: 2
                },
                768: {
                    items: 3
                },
                992: {
                    items: 4
                }
            }
        });
        $('.product_thumbnail_slides').owlCarousel({
            items: 1,
            margin: 0,
            loop: true,
            nav: true,
            navText: ["<img src='img/core-img/long-arrow-left.svg' alt=''>", "<img src='img/core-img/long-arrow-right.svg' alt=''>"],
            dots: false,
            autoplay: true,
            autoplayTimeout: 5000,
            smartSpeed: 1000
        });
    }

    // :: Header Cart Active Code
    var cartbtn1 = $('#essenceCartBtn');
    var cartOverlay = $(".cart-bg-overlay");
    var cartWrapper = $(".right-side-cart-area");
    var cartbtn2 = $("#rightSideCart");
    var cartOverlayOn = "cart-bg-overlay-on";
    var cartOn = "cart-on";

    cartbtn1.on('click', function () {
        cartOverlay.toggleClass(cartOverlayOn);
        cartWrapper.toggleClass(cartOn);
    });
    cartOverlay.on('click', function () {
        $(this).removeClass(cartOverlayOn);
        cartWrapper.removeClass(cartOn);
    });
    cartbtn2.on('click', function () {
        cartOverlay.removeClass(cartOverlayOn);
        cartWrapper.removeClass(cartOn);
    });

    // :: ScrollUp Active Code
    if ($.fn.scrollUp) {
        $.scrollUp({
            scrollSpeed: 1000,
            easingType: 'easeInOutQuart',
            scrollText: '<i class="fa fa-angle-up" aria-hidden="true"></i>'
        });
    }


    // :: Nice Select Active Code
    if ($.fn.niceSelect) {
        $('select').niceSelect();
    }

    // :: Slider Range Price Active Code
    $('.slider-range-price').each(function () {
        var min = jQuery(this).data('min');
        var max = jQuery(this).data('max');
        var unit = jQuery(this).data('unit');
        var value_min = jQuery(this).data('value-min');
        var value_max = jQuery(this).data('value-max');
        var label_result = jQuery(this).data('label-result');
        var t = $(this);
        $(this).slider({
            range: true,
            min: min,
            max: max,
            values: [value_min, value_max],
            slide: function (event, ui) {
                var result = label_result + " " + unit + ui.values[0] + ' - ' + unit + ui.values[1];
                console.log(t);
                t.closest('.slider-range').find('.range-price').html(result);
            }
        });
    });

    // :: Favorite Button Active Code
    var favme = $(".favme");

    favme.on('click', function () {
        $(this).toggleClass('active');
    });

    favme.on('click touchstart', function () {
        $(this).toggleClass('is_animating');
    });

    favme.on('animationend', function () {
        $(this).toggleClass('is_animating');
    });

    // :: Nicescroll Active Code
    if ($.fn.niceScroll) {
        $(".cart-list, .cart-content").niceScroll();
    }

    // :: wow Active Code
    if ($window.width() > 767) {
        new WOW().init();
    }

    // :: Tooltip Active Code
    if ($.fn.tooltip) {
        $('[data-toggle="tooltip"]').tooltip();
    }

    // :: PreventDefault a Click
    $("a[href='#']").on('click', function ($) {
        $.preventDefault();
    });

})(jQuery);

//FILTRO PAGINA PRODUTO
$('.sub-menu.collapse.show li').click(function(){
    $('.shop_grid_product_area .single-product-wrapper').hide();
});
$('.sub-menu.collapse.show li:nth-child(1)').click(function(){
    $('.shop_grid_product_area .single-product-wrapper').each(function() {
        if ($(this).hasClass('at')) {
            $(this).show();
            $('.shop-page .total-products span').text('2');
        }
    });
});
$('.sub-menu.collapse.show li:nth-child(2)').click(function(){
    $('.shop_grid_product_area .single-product-wrapper').each(function() {
        if ($(this).hasClass('bg')) {
            $(this).show();
            $('.shop-page .total-products span').text('2');
        }
    });
});
$('.sub-menu.collapse.show li:nth-child(3)').click(function(){
    $('.shop_grid_product_area .single-product-wrapper').each(function() {
        if ($(this).hasClass('it')) {
            $(this).show();
            $('.shop-page .total-products span').text('3');
        }
    });
});
//OBRIGATORIO CONCORDAR TERMOS
$('input.btn.essence-btn.submit').on('click', function() {
    if(!$('#customCheck1:checked').length) {
        $(this).preventDefault();
        swal({ 
            title: "Selecione os termos",
            text: "Depois confirme"
        }); 
    }
});
//ALERT SWAL
$('.carona input.btn.essence-btn').click(function(){
    swal({
      title: "Sua carona foi solicitada!",
      text: "Seu lugar está reservado",
      icon: "success",
      button: "Confirmar",
    }).then(function() {
        window.location.href = "index.php";
    });
});
//ALERT SWAL

//VISUALIZAR USUARIOS
$('.see-users').on('click', function(){
    $('.table-users').show('300');
});
$('.hide-users').on('click', function(){
    $('.table-users').hide('300');
});

//VERIFICA SENHAS IGUAIS
$('#conf_password, #password').on('change', function(){
    if ($('#conf_password').val() !== $('#password').val()) {
         swal("As senhas não batem");
    }
});

//ESCONDE CASO NAO ESTEJA LOGADO
if ($('button.btn.btn-primary.dropdown-toggle').is(":empty")) {
    $('.navbar').hide();
}

//DELETA USUARIO
function deleteUser() {
    $('.delete-user').fadeIn('300');

    $('.delete-btn-nao, section').on('click', function(){
        $('.delete-user').fadeOut('300');
    });

    $('.delete-btn-sim').on('click', function() {
        window.location.href = 'deletar-usuario.php';
    });
}

//LOGIN NECESSÁRIO
$(document).ready(function(){
    if (window.location.href.indexOf('login=true') != -1) {
        swal("Login necessário para continuar");
    }
});