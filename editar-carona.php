<?php
session_start();
include_once('link.php');

$getID = $_GET["id"];
$nome = $_SESSION['firstname'];

$conn = mysqli_connect("localhost", "root", "","registrationdb");   
$query = mysqli_query($conn,"SELECT * FROM tblcaronacadastro WHERE ID=$getID AND NOME='$nome'");
$row = mysqli_fetch_assoc($query);

if(!$row){
    ?>
    <div>CADASTRO DE CARONA DE OUTRA PESSOA</div>
    <br/>
    <a href="cadastrar-carona.php"> CLIQUE AQUI PARA VOLTAR</a>
<?php
die();
}

?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="description" content="">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- The above 4 meta tags *must* come first in the head; any other head content must come *after* these tags -->

    <!-- Title  -->
    <title>Editar Carona</title>

    <!-- Favicon  -->
    <link rel="icon" href="img/core-img/carona-logo.png">

    <!-- Core Style CSS -->
    <link rel="stylesheet" href="css/core-style.css">
    <link rel="stylesheet" href="style.css">

</head>

<body class="cadastrar-carona-page">
    
    <?php include_once('header.html'); ?>

    <!-- ##### Breadcumb Area Start ##### -->
    <div class="breadcumb_area bg-img" style="background-image: url(img/bg-img/breadcumb.jpg);">
        <div class="container h-100">
            <div class="row h-100 align-items-center">
                <div class="col-12">
                    <div class="page-title text-center">
                        <h2>cadastrar carona</h2>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- ##### Breadcumb Area End ##### -->

    <!-- ##### Checkout Area Start ##### -->
    <div class="checkout_area section-padding-80">
        <div class="container">
            <div class="row">

                <div class="col-12 col-md-6">
                    <div class="checkout_details_area clearfix order-details-confirmation carona">

                        <div class="cart-page-heading mb-30">
                            <h5>Cadastre sua Carona</h5>
                        </div>

                        <form action="editar-carona.php?id=<?php echo $row['ID'];?>" method="post">
                            <input type="hidden" name="id" value="<?php echo $getID;?>">
                            <div class="row">
                                <div class="col-12 mb-3">
                                    <label for="country">Motorista <span>*</span></label>
                                    <input type="text" name="NOME" readonly value="<?php echo $_SESSION['firstname']?>" class="form-control">
                                </div>
                                <div class="col-12 mb-3 ponto-partida">
                                    <label for="country">Ponto de Partida <span>*</span></label>
                                    <select class="w-100" id="pontoPartida" name="pontoPartida">
                                        <option value="Atibaia" <?php echo $row['PONTOPARTIDA'] == 'Atibaia'?'selected' : '';?>>Atibaia</option>
                                        <option value="Bragança Paulista" <?php echo $row['PONTOPARTIDA'] == 'Bragança Paulista'?'selected' : '';?>>Bragança Paulista</option>
                                        <option value="Itatiba" <?php echo $row['PONTOPARTIDA'] == 'Itatiba'?'selected' : '';?>>Itatiba</option>
                                    </select>
                                </div>
                                <div class="col-12 mb-3 ponto-chegada">
                                    <label for="country">Ponto de Chegada <span>*</span></label>
                                    <select class="w-100" id="pontoChegada" name="pontoChegada">
                                        <option value="Atibaia" <?php echo $row['PONTOCHEGADA'] == 'Atibaia'?'selected' : '';?>>Atibaia</option>
                                        <option value="Bragança Paulista" <?php echo $row['PONTOCHEGADA'] == 'Bragança Paulista'?'selected' : '';?>>Bragança Paulista</option>
                                        <option value="Itatiba" <?php echo $row['PONTOCHEGADA'] == 'Itatiba'?'selected' : '';?>>Itatiba</option>
                                    </select>
                                </div>
                                <div class="col-12 mb-3 date-time">
                                    <div class="departure-container" data-container="departure">
                                        <label for="new_publication_step1_departureDate" class=" control-label">
                                            Data da partida:
                                        </label>
                                        <label for="new_publication_step1_departureDate_date" aria-hidden="true"></label>
                                        <div id="new_publication_step1_departureDate" class="departure-date">
                                            <input type="date" name="DATE" id="date" value="<?php echo date_format(date_create($row['DATAPARTIDA']),'Y-m-d');?>" required/>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-12 mb-3 date-time">
                                    <label for="new_publication_step1_departureDate" class=" control-label">
                                        Descrição da viagem: 
                                    </label>
                                    <textarea name="DESCRICAO" id="DESCRICAO" rows="6" cols="60"><?php echo $row['DESCRICAO'];?></textarea>
                                </div>
                                <div class="col-12">
                                    <input type="submit" name="submitCadastrar" class="btn essence-btn cadastro-btn submit"/>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- ##### Checkout Area End ##### -->

    <?php include_once('footer.html'); ?>

    <!-- INSERIR DADOS -->
    <?php

        if (isset($_POST['pontoPartida'])){
            $pPartida = $_POST['pontoPartida'];
            $pChegada = $_POST['pontoChegada'];
            $date = $_POST['DATE'];
            $descricao = $_POST['DESCRICAO'];

            $conn = mysqli_connect("localhost", "root", "","registrationdb");
            $sql = "UPDATE tblcaronacadastro SET PONTOPARTIDA ='$pPartida', PONTOCHEGADA = '$pChegada', DATAPARTIDA = '$date', DESCRICAO = '$descricao' WHERE ID = $getID";

            if ($conn->query($sql) === TRUE) {
                echo "Edit Success";
            } else {
                echo "Error: " . $sql . "<br>" . $conn->error;
            }
            header("Location: cadastrar-carona.php");
        }
    ?>

</body>

</html>