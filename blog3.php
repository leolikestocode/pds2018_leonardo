<?php
session_start();
if (!isset($_SESSION['email'])){
    header("Location: login-cadastro.php?login=true");
}
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="description" content="">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- The above 4 meta tags *must* come first in the head; any other head content must come *after* these tags -->

    <!-- Title  -->
    <title>A importância da viagem outdoor</title>

    <!-- Favicon  -->
    <link rel="icon" href="img/core-img/carona-logo.png">

    <!-- Core Style CSS -->
    <link rel="stylesheet" href="css/core-style.css">
    <link rel="stylesheet" href="style.css">

</head>

<body>
    
    <?php include_once('header.html'); ?>

    <!-- ##### Blog Wrapper Area Start ##### -->
    <div class="single-blog-wrapper">

        <!-- Single Blog Post Thumb -->
        <div class="single-blog-post-thumb">
            <img src="img/bg-img/blbk3.jpg" alt="">
        </div>

        <!-- Single Blog Content Wrap -->
        <div class="single-blog-content-wrapper d-flex">

            <!-- Blog Content -->
            <div class="single-blog--text">
                <h2>A importância da viagem outdoor</h2>
                <p>Viagem é viagem. Cada pessoa&nbsp;um possui um estilo de viajar. Uns gostam de cidades,&nbsp;outros de natureza. Eu prefiro surfar nas duas ondas. Não consigo ficar isolado o tempo todo, mas também acho sufocante viajar só por cidades, por mais lindas que elas sejam. Em minha pequena experiência no mundo das trips, percebi a importância de se ter uma vivência ao ar livre. É um momento indispensável. Sempre procuro conciliar um momento offline&nbsp;–&nbsp;alguns dias em completo isolamento –, e outros em meio à cidade, conhecendo algum destino interessante. Pratico o mochilão em algumas de suas vertentes e sou apaixonado por toda essa ambiguidade mochileira.</p>
                <p>Claro que é gostoso andar por cidades, conhecer pontos turísticos, tirar fotos em lugares badalados, comer comida típica…. Espero nunca deixar este lado turistão de lado. Gosto dos clichês, gosto de conhecer o lado b, gosto de conhecer o máximo que puder. Não existe maneira certa de viajar. A única regra é agradar o espírito.</p>
                <p>Mas o objetivo do post é falar sobre viagem outdoor, com a&nbsp;intenção de mostrar o quanto uma viagem ao ar livre&nbsp;pode ser interessante e reservar experiências&nbsp;incríveis.</p>
                <p>Quando foi que você acampou pela última vez? Ou melhor, quando foi que você tirou uns dias <em>off</em> para fazer uma viagem em meio à natureza?&nbsp;Você se lembra de quando teve que tomar banho de&nbsp;rio por simplesmente estar no “meio do nada”? Talvez você nunca tenha feito isso, mesmo vivendo em um país completamente favorável para o turismo de aventura. Não te culpo. Não está na nossa cultura, não está na cultura do brasileiro fazer atividades outdoor. Mas isso está&nbsp;mudando aos poucos, as pessoas têm dado preferência para viagens mais intensas do que para destinos mais comuns. A viagem de aventura está ganhando força, especialmente nos últimos anos, como mostra uma pesquisa realizada pela&nbsp;<em>Adventure Travel Association</em>, que aponta um crescimento de 50% nos últimos tempos. O dado é interessante.</p>

            </div>

            <!-- Related Blog Post -->
            <div class="related-blog-post">
                <!-- Single Related Blog Post -->
                <div class="single-related-blog-post">
                    <img src="img/bg-img/bl1.jpg" alt="">
                    <a href="blog1.php">
                        <h5>Conselho de uma jovem mochileira: viaje sozinha!</h5>
                    </a>
                </div>
                <!-- Single Related Blog Post -->
                <div class="single-related-blog-post">
                    <img src="img/bg-img/bl2.jpg" alt="">
                    <a href="blog2.php">
                        <h5>Livros de montanha e exploração</h5>
                    </a>
                </div>
                <!-- Single Related Blog Post -->
                <div class="single-related-blog-post">
                    <img src="img/bg-img/bl3.jpg" alt="">
                    <a href="blog3.php">
                        <h5>A importância da viagem outdoor</h5>
                    </a>
                </div>
                <!-- Single Related Blog Post -->
                <div class="single-related-blog-post">
                    <img src="img/bg-img/bl4.jpg" alt="">
                    <a href="blog4.php">
                        <h5>Filmes para inspirar sua viagem de mochilão</h5>
                    </a>
                </div>
            </div>

        </div>
    </div>
    <!-- ##### Blog Wrapper Area End ##### -->

    <?php include_once('footer.html'); ?>

</body>

</html>