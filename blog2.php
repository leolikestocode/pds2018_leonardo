<?php
session_start();
if (!isset($_SESSION['email'])){
    header("Location: login-cadastro.php?login=true");
}
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="description" content="">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- The above 4 meta tags *must* come first in the head; any other head content must come *after* these tags -->

    <!-- Title  -->
    <title>Livros de montanha e exploração</title>

    <!-- Favicon  -->
    <link rel="icon" href="img/core-img/carona-logo.png">

    <!-- Core Style CSS -->
    <link rel="stylesheet" href="css/core-style.css">
    <link rel="stylesheet" href="style.css">

</head>

<body>
    
    <?php include_once('header.html'); ?>

    <!-- ##### Blog Wrapper Area Start ##### -->
    <div class="single-blog-wrapper">

        <!-- Single Blog Post Thumb -->
        <div class="single-blog-post-thumb">
            <img src="img/bg-img/blbk2.jpg" alt="">
        </div>

        <!-- Single Blog Content Wrap -->
        <div class="single-blog-content-wrapper d-flex">

            <!-- Blog Content -->
            <div class="single-blog--text">
                <h2>Livros de montanha e exploração</h2>
                <p>Tão bom quanto curtir o silêncio na montanha é ler um livro que cause&nbsp;sensações de liberdade, paz e aventura. Na minha opinião, livros são verdadeiras ferramentas de estudo&nbsp;e compreensão da vida. E, se bem lidos, podem nos dar boa bagagem de conhecimento – independente do assunto. E claro que &nbsp;o montanhismo se inclui nisso.</p>
                <p>É difícil nos limitarmos a ir apenas para a montanha sem querer mergulhar no assunto. Eu, pelo menos, gosto de saber quem foram os pioneiros, o que fizeram, como fizeram. Enfim, gosto de histórias de aventura, principalmente conhecer os feitos extraordinários, os que marcaram época e os que mudaram o mundo de alguma maneira.</p>
                <p>E já faz alguns&nbsp;anos tenho colecionado livros de montanha e exploração. Como disse, são temas que gosto bastante e que me faz ficar próximo da aventura&nbsp;durante o período que não posso&nbsp;estar. Ou seja, essas obras são ótimas válvulas de escape para aquietar a alma. E a biblioteca&nbsp;que adquiri ao longos dos tempos foi através de pesquisas e também por certo engajamento no mundo do outdoor, mas está longe de ser uma lista completa – e nem tem o objetivo de levantar a bandeira de melhores livros. E preciso dizer que não é uma lista fechada, ainda há&nbsp;uma série de livros que pretendo adquirir. Enfim, se trata apenas de uma relação de títulos que compõem o meu acervo pessoal, que foi construída ao longo dos últimos anos. E que deu vontade de dividir com vocês.</p>

            </div>

            <!-- Related Blog Post -->
            <div class="related-blog-post">
                <!-- Single Related Blog Post -->
                <div class="single-related-blog-post">
                    <img src="img/bg-img/bl1.jpg" alt="">
                    <a href="blog1.php">
                        <h5>Conselho de uma jovem mochileira: viaje sozinha!</h5>
                    </a>
                </div>
                <!-- Single Related Blog Post -->
                <div class="single-related-blog-post">
                    <img src="img/bg-img/bl2.jpg" alt="">
                    <a href="blog2.php">
                        <h5>Livros de montanha e exploração</h5>
                    </a>
                </div>
                <!-- Single Related Blog Post -->
                <div class="single-related-blog-post">
                    <img src="img/bg-img/bl3.jpg" alt="">
                    <a href="blog3.php">
                        <h5>A importância da viagem outdoor</h5>
                    </a>
                </div>
                <!-- Single Related Blog Post -->
                <div class="single-related-blog-post">
                    <img src="img/bg-img/bl4.jpg" alt="">
                    <a href="blog4.php">
                        <h5>Filmes para inspirar sua viagem de mochilão</h5>
                    </a>
                </div>
            </div>

        </div>
    </div>
    <!-- ##### Blog Wrapper Area End ##### -->

    <?php include_once('footer.html'); ?>

</body>

</html>