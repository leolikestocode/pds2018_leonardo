<?php
session_start();
if (!isset($_SESSION['email'])){
    header("Location: login-cadastro.php?login=true");
}
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="description" content="">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- The above 4 meta tags *must* come first in the head; any other head content must come *after* these tags -->

    <!-- Title  -->
    <title>Filmes para inspirar sua viagem de mochilão</title>

    <!-- Favicon  -->
    <link rel="icon" href="img/core-img/carona-logo.png">

    <!-- Core Style CSS -->
    <link rel="stylesheet" href="css/core-style.css">
    <link rel="stylesheet" href="style.css">

</head>

<body>
    
    <?php include_once('header.html'); ?>

    <!-- ##### Blog Wrapper Area Start ##### -->
    <div class="single-blog-wrapper">

        <!-- Single Blog Post Thumb -->
        <div class="single-blog-post-thumb">
            <img src="img/bg-img/blbk4.jpg" alt="">
        </div>

        <!-- Single Blog Content Wrap -->
        <div class="single-blog-content-wrapper d-flex">

            <!-- Blog Content -->
            <div class="single-blog--text">
                <h2>Filmes para inspirar sua viagem de mochilão</h2>
                <p>Sabe aquele sentimento de véspera de viagem? &nbsp;Pois bem, assistir um filme inspirador te proporciona uma sensação bastante parecida.</p>
                <p>Fiz uma listinha dos filmes que aguçam os instintos de qualquer mochileiro. Espero que gostem das sugestões…</p>
                <h5>1 – The Way – O caminho de Santiago de Compostela</h5>
                <p>É um dos melhores filmes do gênero que já assisti. É bastante inspirador e nos faz pensar muito sobre o sentido da vida. Já assisti duas vezes e olha que nem gosto de repetir filmes.</p>
                <h5>2 – A Vida Secreta de Walter Mitty</h5>
                <p>O filme por si só já é uma viagem. Diz muito sobre a vida, sonhos e realizações. Além disso, todo o enredo do filme tem a ver com viagem.</p>
                <h5>3 – A Lendária Expedição Antártica de Shackleton</h5>
                <p>Na verdade é um documentário show de bola e muito encorajador, bastante digno e a história mereceria uma produção de Hollywood. Melhor que o documentário, só o livro.</p>

            </div>

            <!-- Related Blog Post -->
            <div class="related-blog-post">
                <!-- Single Related Blog Post -->
                <div class="single-related-blog-post">
                    <img src="img/bg-img/bl1.jpg" alt="">
                    <a href="blog1.php">
                        <h5>Conselho de uma jovem mochileira: viaje sozinha!</h5>
                    </a>
                </div>
                <!-- Single Related Blog Post -->
                <div class="single-related-blog-post">
                    <img src="img/bg-img/bl2.jpg" alt="">
                    <a href="blog2.php">
                        <h5>Livros de montanha e exploração</h5>
                    </a>
                </div>
                <!-- Single Related Blog Post -->
                <div class="single-related-blog-post">
                    <img src="img/bg-img/bl3.jpg" alt="">
                    <a href="blog3.php">
                        <h5>A importância da viagem outdoor</h5>
                    </a>
                </div>
                <!-- Single Related Blog Post -->
                <div class="single-related-blog-post">
                    <img src="img/bg-img/bl4.jpg" alt="">
                    <a href="blog4.php">
                        <h5>Filmes para inspirar sua viagem de mochilão</h5>
                    </a>
                </div>
            </div>

        </div>
    </div>
    <!-- ##### Blog Wrapper Area End ##### -->

    <?php include_once('footer.html'); ?>

</body>

</html>