<?php
session_start();
if (!isset($_SESSION['email'])){
    header("Location: login-cadastro.php?login=true");
}
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="description" content="">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- The above 4 meta tags *must* come first in the head; any other head content must come *after* these tags -->

    <!-- Title  -->
    <title>Contato</title>

    <!-- Favicon  -->
    <link rel="icon" href="img/core-img/carona-logo.png">

    <!-- Core Style CSS -->
    <link rel="stylesheet" href="css/core-style.css">
    <link rel="stylesheet" href="style.css">

</head>

<body>
    
    <?php include_once('header.html'); ?>

    <div class="contact-area d-flex align-items-center">

        <div class="google-map">
            <img src="img/bg-img/google-maps.png" alt="Foto Satélite"/>
        </div>

        <div class="contact-info">
            <h2>Como nos encontrar?</h2>
            <p>Siga o caminho descrito no Google Maps, e vai conseguir nos encontrar facilmente.</p>

            <div class="contact-address mt-50">
                <p><span>Endereço:</span> Rua: Daniel Silveiras, 255, Atibaia</p>
                <p><span>CEP</span> 12947-590</p>
                <p><span>Celular:</span> +55 11 97249-6173</p>
                <p><a href="mailto:contato@carona10.com.br">contato@carona10.com.br</a></p>
            </div>
        </div>

    </div>

    <?php include_once('footer.html'); ?>

</body>

</html>