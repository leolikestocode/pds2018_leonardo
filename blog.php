<?php
session_start();
if (!isset($_SESSION['email'])){
    header("Location: login-cadastro.php?login=true");
}
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="description" content="">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- The above 4 meta tags *must* come first in the head; any other head content must come *after* these tags -->

    <!-- Title  -->
    <title>Blog</title>

    <!-- Favicon  -->
    <link rel="icon" href="img/core-img/carona-logo.png">

    <!-- Core Style CSS -->
    <link rel="stylesheet" href="css/core-style.css">
    <link rel="stylesheet" href="style.css">

</head>

<body>

    <?php include_once('header.html'); ?>

    <!-- ##### Breadcumb Area Start ##### -->
    <div class="breadcumb_area breadcumb-style-two bg-img" style="background-image: url(img/bg-img/breadcumb2.jpg);">
        <div class="container h-100">
            <div class="row h-100 align-items-center">
                <div class="col-12">
                    <div class="page-title text-center">
                        <h2>Carona Blog</h2>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- ##### Breadcumb Area End ##### -->

    <!-- ##### Blog Wrapper Area Start ##### -->
    <div class="blog-wrapper section-padding-80">
        <div class="container">
            <div class="row">
               
                <!-- Single Blog Area -->
                <div class="col-12 col-lg-6">
                    <div class="single-blog-area mb-50">
                        <img src="img/bg-img/bl1.jpg" alt="">
                        <!-- Post Title -->
                        <div class="post-title">
                            <a href="blog1.php">Conselho de uma jovem mochileira: viaje sozinha!</a>
                        </div>
                        <!-- Hover Content -->
                        <div class="hover-content">
                            <!-- Post Title -->
                            <div class="hover-post-title">
                                <a href="blog1.php">Conselho de uma jovem mochileira: viaje sozinha!</a>
                            </div>
                            <p>Quando eu era criança tinha medo de ir no quintal de casa sozinha. Pois é. Pouca gente sabe disso, mas meus familiares não me deixariam mentir</p>
                            <a href="blog1.php">Continue Lendo<i class="fa fa-angle-right"></i></a>
                        </div>
                    </div>
                </div>
               
                <!-- Single Blog Area -->
                <div class="col-12 col-lg-6">
                    <div class="single-blog-area mb-50">
                        <img src="img/bg-img/bl2.jpg" alt="">
                        <!-- Post Title -->
                        <div class="post-title">
                            <a href="blog2.php">Livros de montanha e exploração</a>
                        </div>
                        <!-- Hover Content -->
                        <div class="hover-content">
                            <!-- Post Title -->
                            <div class="hover-post-title">
                                <a href="blog2.php">Livros de montanha e exploração</a>
                            </div>
                            <p>Tão bom quanto curtir o silêncio na montanha é ler um livro que cause sensações de liberdade, paz e aventura. Na minha opinião, livros são verdadeiras ferramentas de estudo e compreensão da vida.</p>
                            <a href="blog2.php">Continue Lendo<i class="fa fa-angle-right"></i></a>
                        </div>
                    </div>
                </div>
               
                <!-- Single Blog Area -->
                <div class="col-12 col-lg-6">
                    <div class="single-blog-area mb-50">
                        <img src="img/bg-img/bl3.jpg" alt="">
                        <!-- Post Title -->
                        <div class="post-title">
                            <a href="blog3.php">A importância da viagem outdoor</a>
                        </div>
                        <!-- Hover Content -->
                        <div class="hover-content">
                            <!-- Post Title -->
                            <div class="hover-post-title">
                                <a href="blog3.php">A importância da viagem outdoor</a>
                            </div>
                            <p>Viagem é viagem. Cada pessoa um possui um estilo de viajar. Uns gostam de cidades, outros de natureza.</p>
                            <a href="blog3.php">Continue Lendo<i class="fa fa-angle-right"></i></a>
                        </div>
                    </div>
                </div>
               
                <!-- Single Blog Area -->
                <div class="col-12 col-lg-6">
                    <div class="single-blog-area mb-50">
                        <img src="img/bg-img/bl4.jpg" alt="">
                        <!-- Post Title -->
                        <div class="post-title">
                            <a href="blog4.php">Filmes para inspirar sua viagem de mochilão</a>
                        </div>
                        <!-- Hover Content -->
                        <div class="hover-content">
                            <!-- Post Title -->
                            <div class="hover-post-title">
                                <a href="blog4.php">Filmes para inspirar sua viagem de mochilão</a>
                            </div>
                            <p>Sabe aquele sentimento de véspera de viagem? &nbsp;Pois bem, assistir um filme inspirador te proporciona uma sensação bastante parecida.</p>
                            <a href="blog4.php">Continue Lendo<i class="fa fa-angle-right"></i></a>
                        </div>
                    </div>
                </div>
               
            </div>
        </div>
    </div>
    <!-- ##### Blog Wrapper Area End ##### -->

    <?php include_once('footer.html'); ?>

</body>

</html>